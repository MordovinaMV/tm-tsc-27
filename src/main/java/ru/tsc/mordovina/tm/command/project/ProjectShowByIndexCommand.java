package ru.tsc.mordovina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractProjectCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        @Nullable final Integer index = TerminalUtil.nextNumber();
        @NotNull final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        showProject(project);
    }

}
