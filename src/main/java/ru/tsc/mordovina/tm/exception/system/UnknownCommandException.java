package ru.tsc.mordovina.tm.exception.system;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Incorrect " + command + " command.");
    }

}
