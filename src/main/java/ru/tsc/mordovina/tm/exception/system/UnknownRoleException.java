package ru.tsc.mordovina.tm.exception.system;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class UnknownRoleException extends AbstractException {

    public UnknownRoleException(final String role) {
        super("Error. Role `" + role + "` not found.");
    }

}
